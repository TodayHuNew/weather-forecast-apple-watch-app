# Weather Forecast - Apple Watch App
This Demo app is designed for the  Watch. This is very simple weather app and  exceptionally easy to use app for staying always updated with the weather conditions.  
There is a Storyboard which is the Watchkit app, and a set of Interface Controllers that contain the logic.

![weatherAppDemo.jpg](https://bitbucket.org/repo/jyMBnM/images/3043313501-weatherAppDemo.jpg)


![Screen Shot 2015-04-21 at 6.13.40 pm.png](https://bitbucket.org/repo/jyMBnM/images/1026571535-Screen%20Shot%202015-04-21%20at%206.13.40%20pm.png)
#How to build with app container support

1.	Select the **WatchAppDemo** project in the Xcode project navigator

2.	Update all three targets below to use your own Team:
o	WatchAppDemo- the WatchAppDemo app for iOS
o	**WatchAppDemo WatchKit Extension** - the WatchKit extension for Apple Watch
o	**WatchAppDemo WatchKit App** - the WatchKit app for Apple Watch

3.	Now switch to **WatchAppDemo** target and select the **Capabilities** tab. Enable **App Groups** by flicking the switch to **On**.
Make sure the **App Groups** section is expanded, and tap the + button to add a new group. 
Name it group.<YOUR_DOMAIN>.<GROUP_NAME>. ex: group.com.domain.weatherapp
then slelect the group you just created.
4.	Next, enable app groups by repeating the same steps for the WatchAppDemo WatchKit Extension target. This time, all you have to do is select the App Group you just created.
5.	Update the Bundle Identifier of all three targets, such as:
o	**WatchAppDemo** - com. domain. weatherapp
o	**WatchAppDemo WatchKit Extension** - com.domain. weatherapp.watchkit
o	**WatchAppDemo WatchKit App** - com.domain.weatherapp
Then tap "Fix Issue" to let Xcode help you!
6.	Update **GlanceController.m** and **InterfaceController.m**, change the value of **kAPPGROUPIDENTIFIER**: group.com.domain.weatherapp
7.	Select **WatchKit Extension's info.plist**, then **changeNSExtension/NSExtensionAttributes/WKAppBundleIdentifier** to **WatchAppDemo WatchKit App's** Bundle Identifier.
8.	Select Product -> Clean, then change Schema to WatchAppDemo Watchkit App and you are ready to run!

#Third Party Libraries

https://github.com/comyarzaheri/CZWeatherKit

https://github.com/litoarias/HACLocationManager