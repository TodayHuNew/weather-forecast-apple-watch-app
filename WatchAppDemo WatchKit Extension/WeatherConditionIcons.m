//
//  WeatherConditionIcons.m
//  WatchAppDemo
//
//  Created by Aamer Khalid on 19/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import "WeatherConditionIcons.h"
@implementation WeatherConditionIcons


+(NSString*)getWeatherConditionIcon:(Climacon)climacon
{

    if(climacon == ClimaconSun) {

    return @"sunny-icon.png";
    
    } else if(climacon == ClimaconCloud) {

    return @"cloudy-icon.png";
    
    } else if(climacon == ClimaconDrizzle) {

    
        return @"lightrain-icon";
        
    } else if(climacon == ClimaconShowers) {
   
        return @"lightrain-icon";
    
    } else if(climacon == ClimaconRain) {
        return @"hail-icon.png";
    
    } else if (climacon == ClimaconHail) {
        return @"lightrain-icon";

    } else if(climacon == ClimaconSnow) {
        return @"lightrain-icon";

    } else if(climacon == ClimaconFog) {

    } else if (climacon == ClimaconHaze) {

    
    }
    
    return @"";
}

+(NSString*)getBackgroundWRTWeatherCondition:(Climacon)climacon
{
    
    if(climacon == ClimaconSun) {
        
        return @"sunshine-watchweatherbg.png";
        
    } else if(climacon == ClimaconCloud) {
        
        return @"cloudy-watchweatherbg.png";
        
    } else if(climacon == ClimaconDrizzle) {
        
        
        return @"raining-watchbg.png";
        
    } else if(climacon == ClimaconShowers) {
        
        return @"raining-watchbg.png";
        
    } else if(climacon == ClimaconRain) {
        return @"raining-watchbg.png";
        
    } else if (climacon == ClimaconHail) {
        return @"raining-watchbg.png";
        
    } else if(climacon == ClimaconSnow) {
        return @"raining-watchbg.png";
        
    } else if(climacon == ClimaconFog) {
        
    } else if (climacon == ClimaconHaze) {
        
        
    }
    
    return @"";
}

+ (Climacon)climaconCharacterForDescription:(NSString *)description
{
    Climacon icon = ClimaconSun;
    NSString *lowercaseDescription = description.lowercaseString;
    
    if([lowercaseDescription cz_contains:@"clear"]) {
        icon = ClimaconSun;
    } else if([lowercaseDescription cz_contains:@"cloud"]) {
        icon = ClimaconCloud;
    } else if([lowercaseDescription cz_contains:@"drizzle"]) {
        icon = ClimaconDrizzle;
    } else if([lowercaseDescription cz_contains:@"showers"]) {
        icon = ClimaconShowers;
    } else if([lowercaseDescription cz_contains:@"rain"] ||
              [lowercaseDescription cz_contains:@"thunderstorm"]) {
        icon = ClimaconRain;
    } else if ([lowercaseDescription cz_contains:@"hail"]) {
        icon = ClimaconHail;
    } else if([lowercaseDescription cz_contains:@"snow"] ||
              [lowercaseDescription cz_contains:@"ice"]) {
        icon = ClimaconSnow;
    } else if([lowercaseDescription cz_contains:@"fog"]) {
        icon = ClimaconFog;
    } else if ([lowercaseDescription cz_contains:@"overcast"] ||
               [lowercaseDescription cz_contains:@"smoke"]    ||
               [lowercaseDescription cz_contains:@"dust"]     ||
               [lowercaseDescription cz_contains:@"ash"]      ||
               [lowercaseDescription cz_contains:@"mist"]     ||
               [lowercaseDescription cz_contains:@"haze"]     ||
               [lowercaseDescription cz_contains:@"spray"]    ||
               [lowercaseDescription cz_contains:@"squall"]) {
        icon = ClimaconHaze;
    }
    return icon;
}

@end
