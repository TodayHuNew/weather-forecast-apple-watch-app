//
//  InterfaceController.m
//  WatchAppDemo WatchKit Extension
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import "InterfaceController.h"
#define kAPPGROUPIDENTIFIER @"group.com.swenggco.weatherapp"
@interface InterfaceController()

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"EEE";
    
    
    NSURL *directory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAPPGROUPIDENTIFIER];
    
    NSDictionary *weatherDictionary=[CDPlistManager readFromFile:@"weather" filePath:[directory path]];
    
    [self.locationLabel setText:weatherDictionary[@"location"]];
    
    [self.temperaturIconLabel setText:weatherDictionary[@"temperatureIcon"]];
    
    [self.weatherConditionDescription setText:weatherDictionary[@"temperatureCondiion"]];
    
    [self.temeratureLabel setText:weatherDictionary[@"temperature"]];
    
    
    Climacon climacon = [WeatherConditionIcons climaconCharacterForDescription:weatherDictionary[@"temperatureCondiion"]];
   
    [self.mainGroup setBackgroundImage:[UIImage imageNamed:[WeatherConditionIcons getBackgroundWRTWeatherCondition:climacon]]];
    
    NSString *iconName=[WeatherConditionIcons getWeatherConditionIcon:climacon];
    
    [self.weatherImageView setImage:[UIImage imageNamed:iconName]];
    
    //Set Forcast Days
    [self.forcastDayOneLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:86400]]];
    [self.forcastDayTwoLabel setText: [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:172800]]];
    [self.forcastDayThreeLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:259200]]];

    // Set Forcast icons
    [self.forcastDayOneImage setImage:[UIImage imageNamed:[WeatherConditionIcons getWeatherConditionIcon: [WeatherConditionIcons climaconCharacterForDescription:weatherDictionary[@"forcastDay1"]]]]];
    
    [self.forcastDayTwoImage setImage:[UIImage imageNamed:[WeatherConditionIcons getWeatherConditionIcon: [WeatherConditionIcons climaconCharacterForDescription:weatherDictionary[@"forcastDay2"]]]]];
    
    [self.forcastDayThreeImage setImage:[UIImage imageNamed:[WeatherConditionIcons getWeatherConditionIcon: [WeatherConditionIcons climaconCharacterForDescription:weatherDictionary[@"forcastDay3"]]]]];
    
    

}


- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



