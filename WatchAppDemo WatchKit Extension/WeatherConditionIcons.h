//
//  WeatherConditionIcons.h
//  WatchAppDemo
//
//  Created by Aamer Khalid on 19/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Climacons.h"
#import "NSString+CZWeatherKit_Substring.h"

@interface WeatherConditionIcons : NSObject
+(NSString*)getWeatherConditionIcon:(Climacon)climacon;
+(NSString*)getBackgroundWRTWeatherCondition:(Climacon)climacon;
+ (Climacon)climaconCharacterForDescription:(NSString *)description;
@end
