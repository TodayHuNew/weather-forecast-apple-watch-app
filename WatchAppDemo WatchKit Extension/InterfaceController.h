//
//  InterfaceController.h
//  WatchAppDemo WatchKit Extension
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>
#import "WeatherConditionIcons.h"
#import "Climacons.h"
#import "CDPlistManager.h"
#import "NSString+CZWeatherKit_Substring.h"

@interface InterfaceController : WKInterfaceController


@property (nonatomic,weak) NSArray *dataArray;

#pragma mark - IBOutlet
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *mainGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *locationLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *temperaturIconLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *weatherImageView;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *weatherConditionDescription;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *temeratureLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *forcastDayOneLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *forcastDayTwoLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *forcastDayThreeLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *forcastDayOneImage;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *forcastDayTwoImage;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *forcastDayThreeImage;

@end
