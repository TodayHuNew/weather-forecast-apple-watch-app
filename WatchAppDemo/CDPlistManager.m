//
//  CDPlistManager.m
//
//  Created by Aamer Khalid on 21/10/2014.
//  Copyright (c) 2014 Swenggco Software. All rights reserved.
//

#import "CDPlistManager.h"
#define kAPPGROUPIDENTIFIER @"group.com.swenggco.weatherapp"

@implementation CDPlistManager 

// create file in root folder (./document/)
+(void)createFile:(NSString *)fileName

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:newPlistFile];
    if (!fileExists) {
        NSMutableArray * array = [[NSMutableArray alloc]init];
        [array writeToFile:newPlistFile atomically:YES];
    }
  
}

// create file in custom folder (./document/path/)
+(void)createFile:(NSString *)fileName filePath:(NSString *)path

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * pathFolder = [NSString stringWithFormat:@"%@/%@/", documentFolder,path];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
    
    NSFileManager *defaultManager;
    defaultManager = [NSFileManager defaultManager];
    
    if (![defaultManager fileExistsAtPath:path])
        [defaultManager createDirectoryAtPath:pathFolder withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    [array writeToFile:newPlistFile atomically:YES];
    
}

//  create file with data in root folder (./document/)
+(void)createFile:(NSString *)fileName dataToFile:(NSMutableDictionary*)data

{
    NSURL *directory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAPPGROUPIDENTIFIER];
    
    NSString *documentFolder = [directory path] ;
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:newPlistFile];
    if (!fileExists) {
    [data writeToFile:newPlistFile atomically:YES];
    
    }
}

//  create file with data in custom folder (./document/path)
+(void)createFile:(NSString *)fileName filePath:(NSString *)path dataToFile:(NSMutableDictionary *)data

{
    
        NSURL *directory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAPPGROUPIDENTIFIER];
   
    NSString *documentFolder = [directory path] ;
        
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
    
    NSFileManager *defaultManager;
    defaultManager = [NSFileManager defaultManager];
    
    if (![defaultManager fileExistsAtPath:path])
    {
        [defaultManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    
    [data writeToFile:newPlistFile atomically:YES];
    }
    
    
}


// write to file
+(void)writeToFile:(NSString *)fileName dataToFile:(NSMutableDictionary *)data

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    [data writeToFile:newPlistFile atomically:YES]; 
    
    
}

// write to file on the path
+(void)writeToFile:(NSString *)fileName dataToFile:(NSMutableDictionary *)data filePath:(NSString *)path

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * pathFolder = [NSString stringWithFormat:@"%@/%@/", documentFolder,path];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
    
    NSFileManager *defaultManager;
    defaultManager = [NSFileManager defaultManager];
    
    if (![defaultManager fileExistsAtPath:path])
        [defaultManager createDirectoryAtPath:pathFolder withIntermediateDirectories:YES attributes:nil error:nil];
    
    [data writeToFile:newPlistFile atomically:YES];
}


// read from file
+(NSDictionary*)readFromFile:(NSString *)fileName

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    NSString *plistPath = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    return dict;
}

// read from file in root folder (.plist created in the project root)
+(NSMutableArray*)readFromResourcePlist:(NSString*)namePlist
{
    NSString *path = [[NSBundle mainBundle] pathForResource:
                      namePlist ofType:@"plist"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:path];
    for (NSString *str in array)
        NSLog(@"--%@", str);
    
    return array;
}

// read from file on the path
+(NSDictionary*)readFromFile:(NSString *)fileName filePath:(NSString *)path

{
    NSURL *directory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAPPGROUPIDENTIFIER];
    
    NSString *plistPath=[[directory path] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    NSFileManager *defaultManager;
    defaultManager = [NSFileManager defaultManager];
    
    if (![defaultManager fileExistsAtPath:path])
        [defaultManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    return dict;
    
}


// output file names are on the way "./document/path"
+(NSArray*)checkFileFromFolder:(NSString *)path
{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@/",documentFolder,path] error:nil];
    
    for (int i = 0; i<[dirContents count ]; i++) {
        NSLog(@"document  = %@",[dirContents objectAtIndex:i]);
    }
    
    return dirContents;
    
}

// delete file
+(void)deleteFile:(NSString *)filePath

{
    
    NSError *error;
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * result = @"";
    result = [result stringByAppendingFormat:@"%@",documentFolder];
    result = [result stringByAppendingFormat:@"/"];
    result = [result stringByAppendingFormat:@"%@",filePath];
    if ([[NSFileManager defaultManager] removeItemAtPath:result error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
}

//Estrae un valore dalla plist passata come primo parametro
+ (id)valorePlist:(NSString *)nomeFile conChiave:(NSString*)chiave {
    
    NSDictionary *dictionary =[self readFromFile:nomeFile];
    
    
    
    return dictionary[chiave];
}
+(void)modificaStringaPlist:(NSString *)nomeFile conChiave:(NSString*)chiave eStringa:(id)stringa{
    
   
    
    NSURL *directory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAPPGROUPIDENTIFIER];
    
    NSString *plistPath=[[directory path] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", nomeFile]];
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: plistPath]) {
        NSMutableDictionary *tempPlist = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
        
        [tempPlist setObject:stringa forKey:chiave];
        [tempPlist writeToFile:plistPath atomically:YES];
    } else {
        NSLog(@"NRSimplePlist - Errore nella MODIFICA STRINGA del file plist '%s'", [plistPath UTF8String],nil);
        NSLog(@"NRSimplePlist - Error EDITING STRING of plist file '%s'", [plistPath UTF8String],nil);
    }
}

/*!
 * @discussion Getting Value From Plist from Key Value
 * @param plistName
 * @param key
 * @return NSdictionary
 */
+(id)valuePlist:(NSString *)plistName withKey:(NSString*)key{
    return [self valorePlist:plistName conChiave:key];
}
/*!
 * @discussion update the plist values with respect to key
 * @param plistName
 * @param key
 * @param string
 * @return nil
 */
+(void)editStringPlist:(NSString *)plistName withKey:(NSString*)key andString:(id)string{
    [self modificaStringaPlist:plistName conChiave:key eStringa:string];
}

@end
