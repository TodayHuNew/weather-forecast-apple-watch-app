//
//  CZWeatherView.h
//  Copyright (c) 2014, Comyar Zaheri, http://comyar.io
//  All rights reserved.



#pragma mark - CZWeatherView Interface

/**
 CZWeatherView is used to display weather data for a single location to the user.
 */
@interface CZWeatherView : UIView <UIGestureRecognizerDelegate>

// -----
// @name Properties
// -----

#pragma mark Properties

//  Displays the time the weather data for this view was last updated
@property (strong, nonatomic) UILabel                 *updatedLabel;

//  Displays the icon for current conditions
@property (strong, nonatomic) UILabel                 *conditionIconLabel;

//  Displays the description of current conditions
@property (strong, nonatomic) UILabel                 *conditionDescriptionLabel;

//  Displays the location whose weather data is being represented by this weather view
@property (strong, nonatomic) UILabel                 *locationLabel;

//  Displayes the current temperature
@property (strong, nonatomic) UILabel                 *currentTemperatureLabel;

//  Displays both the high and low temperatures for today
@property (strong, nonatomic) UILabel                 *hiloTemperatureLabel;

//  Displays the day of the week for the first forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastDayOneLabel;

//  Displays the day of the week for the second forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastDayTwoLabel;

//  Displays the day of the week for the third forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastDayThreeLabel;

//  Displays the day of the week for the third forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastDayFourLabel;
//  Displays the day of the week for the third forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastDayFiveLabel;



//  Displays the icon representing the predicted conditions for the first forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastIconOneLabel;

//  Displays the icon representing the predicted conditions for the second forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastIconTwoLabel;

//  Displays the icon representing the predicted conditions for the third forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastIconThreeLabel;

//  Displays the icon representing the predicted conditions for the Fourth forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastIconFourLabel;
//  Displays the icon representing the predicted conditions for the Fifth forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastIconFiveLabel;



//  Displays the icon representing the predicted conditions for the first forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastOnehighLowTemperatureLabel;

//  Displays the icon representing the predicted conditions for the second forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastTwohighLowTemperatureLabel;

//  Displays the icon representing the predicted conditions for the third forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastThreehighLowTemperatureLabel;

//  Displays the icon representing the predicted conditions for the Fourth forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastFourhighLowTemperatureLabel;
//  Displays the icon representing the predicted conditions for the Fifth forecast snapshot
@property (strong, nonatomic) UILabel                 *forecastFivehighLowTemperatureLabel;


//  Indicates whether data is being downloaded for this weather view
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end
