//
//  ViewController.m
//  WatchAppDemo
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import "ViewController.h"
#import "CZWeatherKit.h"
#import "CZWeatherView.h"
#import "CDPlistManager.h"

@interface ViewController ()
@property (nonatomic) CZWeatherView     *weatherView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    self.weatherView = [[CZWeatherView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:self.weatherView];
    [self.weatherView.activityIndicator startAnimating];

    
    CZWeatherRequest *request = [CZWeatherRequest requestWithType:CZCurrentConditionsRequestType];
    request.service = [CZOpenWeatherMapService new];

    HACLocationManager *locationManager = [HACLocationManager sharedInstance];
    locationManager.timeoutUpdating =3;
    [locationManager LocationQuery];
    locationManager.locationUpdatedBlock = ^(CLLocation *location){
        NSLog(@"%@", location);
    };
    

    locationManager.geocodingErrorBlock = ^(NSError *error){
        NSLog(@"%@", error);
    };
    locationManager.locationEndBlock = ^(CLLocation *location){
        
       
        
        if (location==nil) {
            location=locationManager.getLastSavedLocation;
        }
        
        
          [CDPlistManager editStringPlist:@"weather" withKey:@"longitude" andString:[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
        
        [CDPlistManager editStringPlist:@"weather" withKey:@"latitude" andString:[NSString stringWithFormat:@"%f",location.coordinate.latitude]];

        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        [geocoder reverseGeocodeLocation:location
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                           
                           if (error){
                               NSLog(@"Geocode failed with error: %@", error);
                               return;
                           }

                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                        
                           self.weatherView.locationLabel.text=[NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.ISOcountryCode];
                           
                        
              [CDPlistManager editStringPlist:@"weather" withKey:@"location" andString:[NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.ISOcountryCode]];
                           
                       }];
        request.location = [CZWeatherLocation locationWithCLLocation:location];
        
        [request performRequestWithHandler:^(id data, NSError *error) {
            if (data) {
                __block CZWeatherCondition *condition = (CZWeatherCondition *)data;
                CZWeatherRequest *request = [CZWeatherRequest requestWithType:CZForecastRequestType];
                request.service = [CZOpenWeatherMapService new];
                request.location = [CZWeatherLocation locationWithCLLocation:location];
                 
                
                [request performRequestWithHandler:^(id data, NSError *error) {
                    if (data) {
                        
                        // Current Conditions
                        self.weatherView.currentTemperatureLabel.text= [NSString stringWithFormat:@"%.0f°", condition.temperature.c];
                        self.weatherView.hiloTemperatureLabel.text = [NSString stringWithFormat:@"H %.0f / L %.0f", condition.highTemperature.c,
                                                                      condition.lowTemperature.c];
                      
                        NSString *temperatureIcon = [NSString stringWithFormat:@"%c", condition.climaconCharacter];
                      
                        [self.backgroundImageView setImage:[UIImage imageNamed:[self getBackgroundWRTWeatherCondition:condition.climaconCharacter]]];
                        [CDPlistManager editStringPlist:@"weather" withKey:@"temperatureIcon" andString:temperatureIcon];
                        self.weatherView.conditionIconLabel.text = temperatureIcon;
                        if ([temperatureIcon isEqualToString:@"I"]) {
                            
                            self.weatherView.conditionIconLabel.textColor=[UIColor colorWithRed:0.986 green:0.780 blue:0.000 alpha:1.000];
                        }
                        
                        
                        self.weatherView.conditionDescriptionLabel.text = [condition.summary capitalizedString];
                        
                        //Save Weather condition into plist
                    [CDPlistManager editStringPlist:@"weather" withKey:@"temperatureCondiion" andString:[condition.summary capitalizedString]];

                    [CDPlistManager editStringPlist:@"weather" withKey:@"temperature" andString:[NSString stringWithFormat:@"%.0f°", condition.temperature.c]];

                    [CDPlistManager editStringPlist:@"weather" withKey:@"High" andString:[NSString stringWithFormat:@"%.0f°", condition.highTemperature.c]];

                    [CDPlistManager editStringPlist:@"weather" withKey:@"Low" andString:[NSString stringWithFormat:@"%.0f°", condition.lowTemperature.c]];
                        
                        // Forecast
                        NSArray *forecasts = (NSArray *)data;
                        [CDPlistManager editStringPlist:@"weather" withKey:@"forcast" andString:(NSArray *)data];
                        
                        
                        if ([forecasts count] >= 5) {
                            
                            
                            NSMutableDictionary *weatherDictionary=[NSMutableDictionary new];
                            
                            
                            NSDateFormatter *dateFormatter = [NSDateFormatter new];
                            dateFormatter.dateFormat = @"EEEE";

                            //Tommorrow Weather
                            NSString *iconOne = [NSString stringWithFormat:@"%c", ((CZWeatherCondition *)forecasts[0]).climaconCharacter];
                            self.weatherView.forecastDayOneLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:86400]];
                            self.weatherView.forecastIconOneLabel.text = iconOne;
                            
                            if ([iconOne isEqualToString:@"I"]) {
                                
                                self.weatherView.forecastIconOneLabel.textColor=[UIColor colorWithRed:0.986 green:0.780 blue:0.000 alpha:1.000];
                            }
                            
                            self.weatherView.forecastOnehighLowTemperatureLabel.text=[NSString stringWithFormat:@"%.0f\u00B0/%.0f\u00B0", ((CZWeatherCondition *)forecasts[0]).highTemperature.c,((CZWeatherCondition *)forecasts[0]).lowTemperature.c] ;
                            
                            
                            //Save forcast summry into plist
                              [CDPlistManager editStringPlist:@"weather" withKey:@"forcastDay1" andString:((CZWeatherCondition *)forecasts[0]).summary];
                              [CDPlistManager editStringPlist:@"weather" withKey:@"forcastDay2" andString:((CZWeatherCondition *)forecasts[1]).summary];
                              [CDPlistManager editStringPlist:@"weather" withKey:@"forcastDay3" andString:((CZWeatherCondition *)forecasts[2]).summary];
                            
                            
 
                            //Day two weather
                            NSString *iconTwo = [NSString stringWithFormat:@"%c", ((CZWeatherCondition *)forecasts[1]).climaconCharacter];
                            self.weatherView.forecastDayTwoLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:172800]];
                            if ([iconTwo isEqualToString:@"I"]) {
                                
                               self.weatherView.forecastIconTwoLabel.textColor=[UIColor colorWithRed:0.986 green:0.780 blue:0.000 alpha:1.000];
                            }
                            self.weatherView.forecastIconTwoLabel.text = iconTwo;
                            [weatherDictionary setObject:(CZWeatherCondition *)forecasts[1] forKey:@"forcast1"];
                            
                            self.weatherView.forecastTwohighLowTemperatureLabel.text=[NSString stringWithFormat:@"%.0f\u00B0/%.0f\u00B0", ((CZWeatherCondition *)forecasts[1]).highTemperature.c,((CZWeatherCondition *)forecasts[1]).lowTemperature.c] ;
                            
                            // Third Day Weather
                            
                            NSString *iconThree = [NSString stringWithFormat:@"%c", ((CZWeatherCondition *)forecasts[2]).climaconCharacter];
                            self.weatherView.forecastDayThreeLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:259200]];
                            self.weatherView.forecastIconThreeLabel.text = iconThree;
                            if ([iconThree isEqualToString:@"I"]) {
                                
                                self.weatherView.forecastIconThreeLabel.textColor=[UIColor colorWithRed:0.986 green:0.780 blue:0.000 alpha:1.000];
                            }
                            [weatherDictionary setObject:(CZWeatherCondition *)forecasts[2] forKey:@"forcast2"];
                            
                            self.weatherView.forecastThreehighLowTemperatureLabel.text=[NSString stringWithFormat:@"%.0f\u00B0/%.0f\u00B0", ((CZWeatherCondition *)forecasts[2]).highTemperature.c,((CZWeatherCondition *)forecasts[2]).lowTemperature.c] ;
//                            self.weatherConditionObject.forcast=weatherDictionary;
                            
                            
                      
                            // Day Four Weather
                            NSString *iconFour = [NSString stringWithFormat:@"%c", ((CZWeatherCondition *)forecasts[3]).climaconCharacter];
                            self.weatherView.forecastDayFourLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:345600]];
                            self.weatherView.forecastIconFourLabel.text = iconFour;
                            if ([iconFour isEqualToString:@"I"]) {
                                
                                self.weatherView.forecastIconFourLabel.textColor=[UIColor colorWithRed:0.986 green:0.780 blue:0.000 alpha:1.000];
                            }
                            
                            
                            self.weatherView.forecastFourhighLowTemperatureLabel.text=[NSString stringWithFormat:@"%.0f\u00B0/%.0f\u00B0", ((CZWeatherCondition *)forecasts[3]).highTemperature.c,((CZWeatherCondition *)forecasts[3]).lowTemperature.c] ;
                            
                            
                            
                            //Day Five Weather
                            
                            NSString *iconFive = [NSString stringWithFormat:@"%c", ((CZWeatherCondition *)forecasts[4]).climaconCharacter];
                            self.weatherView.forecastDayFiveLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:432000]];
                            self.weatherView.forecastIconFiveLabel.text = iconFive;
                            
                            if ([iconFive isEqualToString:@"I"]) {
                                
                                self.weatherView.forecastIconFiveLabel.textColor=[UIColor colorWithRed:0.986 green:0.780 blue:0.000 alpha:1.000];
                            }
                            
                            self.weatherView.forecastFivehighLowTemperatureLabel.text=[NSString stringWithFormat:@"%.0f\u00B0/%.0f\u00B0", ((CZWeatherCondition *)forecasts[4]).highTemperature.c,((CZWeatherCondition *)forecasts[4]).lowTemperature.c] ;
                            
                            
                        }
                        
                        // Updated
                        NSString *updated = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                                           dateStyle:NSDateFormatterMediumStyle
                                                                           timeStyle:NSDateFormatterShortStyle];
                        self.weatherView.updatedLabel.text = [NSString stringWithFormat:@"Updated %@", updated];
                    }
                    [self.weatherView.activityIndicator stopAnimating];
                }];
            } else {
                [self.weatherView.activityIndicator stopAnimating];
            }
        }];

    };
    
}
-(NSString*)getBackgroundWRTWeatherCondition:(Climacon)climacon
{
    
    if(climacon == ClimaconSun) {
        
        return @"sunshine-weatherbg.png";
        
    } else if(climacon == ClimaconCloud) {
        
        return @"cloudy-weatherbg.png";
        
    } else if(climacon == ClimaconDrizzle) {
        
        
        return @"raining-bg.png";
        
    } else if(climacon == ClimaconShowers) {
        
        return @"raining-bg.png";
        
    } else if(climacon == ClimaconRain) {
        return @"raining-bg.png";
        
    } else if (climacon == ClimaconHail) {
        return @"raining-bg.png";
        
    } else if(climacon == ClimaconSnow) {
        return @"raining-bg.png";
        
    } else if(climacon == ClimaconFog) {
        
    } else if (climacon == ClimaconHaze) {
        
        
    }
    
    return @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
