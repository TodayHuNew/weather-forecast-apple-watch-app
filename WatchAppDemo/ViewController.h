//
//  ViewController.h
//  WatchAppDemo
//
//  Created by Aamer Khalid on 10/04/2015.
//  Copyright (c) 2015 Swenggco Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HACLocationManager.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *graphContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *graphImageView;

@end

